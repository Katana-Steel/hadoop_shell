#!/usr/bin/env python3
"""
    Hadoop Distributed Filesystem shell (hsh)
    written by Rene Kjellerup (c) 2018 <rk.katana.steel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import pyarrow as pa
import os.path
import readline
import atexit


hdfs = None
perm_modes = [
    '---',
    '--x',
    '-w-',
    '-wx',
    'r--',
    'r-x',
    'rw-',
    'rwx'
]


def stats(path):
    p = path.replace('//', '/')
    s = hdfs.info(p)
    octal_perms = '{0:o}'.format(s['permissions'])
    s['str_perm'] = ''.join([perm_modes[int(e)] for e in octal_perms])
    print(u'{kind:1.1}{str_perm} {owner}:{group} {size:8} {path}'.format(**s))


def ls(base, args=[]):
    if isinstance(args, str) and len(args) > 0:
        args = [args]
    if len(args) > 0:
        for ar in args:
            if hdfs.exists(ar):
                ls(ar)
                continue
            if hdfs.exists('/'.join([base, ar]).replace('//', '/')):
                ls('/'.join([base, ar]).replace('//', '/'))
    else:
        try:
            for a, b, c in hdfs.walk(base):
                for d in b:
                    stats('/'.join([a, d]))
                for f in c:
                    stats('/'.join([a, f]))
                break
        except Exception as e:
            print(str(e))


def unlink(fname, recurse):
    if hdfs.isdir(fname):
        try:
            hdfs.delete(fname, recurse)
        except Exception:
            print("can't delete non empty directory {0}".format(fname))
    else:
        hdfs.delete(fname)

def delete(base, args):
    if isinstance(args, str) and len(args) > 0:
        args = [args]
    recurse = False
    if '-r' in args:
        recurse = True
    for fname in args:
        if hdfs.exists(fname):
            unlink(fname, recurse)
        if hdfs.exists('/'.join([base, fname]).replace('//', '/')):
            unlink('/'.join([base, fname]).replace('//', '/'), recurse)


def move(base, files):
    if len(files) < 2:
        print('usage: mv src1 [src2 ... srcN] dest')
        return
    dest = files[-1].split('/')
    if hdfs.exists('/'.join(dest)) and not hdfs.isdir('/'.join(dest)):
        print('target exists already')
        return
    elif not hdfs.exists('/'.join(dest)):
        hdfs.mkdir('/'.join(dest))
    for i in files[:-1]:
        src = i.split('/')
        if hdfs.exists(i):
            hdfs.mv(i, '/'.join(dest[:].append(src[-1])))
        elif hdfs.exists('/'.join([base] + src)):
            hdfs.mv('/'.join([base] + src),
                    '/'.join(dest[:].append(src[-1])))
        else:
            print("couldn't find '{0}'".format(i))


def chdir(current, new_dir):
    for dir in new_dir:
        if dir.startswith('/') and hdfs.isdir(dir):
            return dir
        else:
            base = '/'.join([current, dir]).replace('//', '/')
            if hdfs.isdir(base):
                return base
            else:
                print("can't find {0}".format(dir))
                return current


def echo(b, o):
    a = ' '.join(o)
    for k, v in environ.items():
        a = a.replace('${' + k + '}', v)
    print(a)


cmds = {
    'ls': ls,
    'rm': delete,
    'mv': move,
    'echo': echo
}

state = {
    'hdfs_path': '/',
    'os_path': os.path.curdir
}

environ = {}


def cli():
    global hdfs
    print('Hadoop Distributed Filesystem shell version 0.2')
    print('published under GNU GPL version 3 or later')
    accept_input = True
    base = '/'
    if hdfs is None:
        hdfs = pa.hdfs.connect()
    s = hdfs.info(base)
    cluster = s['path'].split(':')[1].replace('/', '')
    prompt = cluster + '\n{0}\n $ '
    while accept_input:
        try:
            cmd = input(prompt.format(base)).split()
        except EOFError:
            accept_input = False
            print('\ngoodbye')
            continue
        try:
            if cmd[0] in cmds:
                if len(cmd) > 2:
                    cmds[cmd[0]](base, cmd[1:])
                elif len(cmd) == 2:
                    cmds[cmd[0]](base, cmd[1])
                else:
                    cmds[cmd[0]](base)
        except Exception:
            print("bad options to '{0}'".format(cmd[0]))
            continue
        if cmd[0] == 'exit':
            accept_input = False
            print('goodbye')
            continue
        if cmd[0] == 'cd':
            base = chdir(base, cmd[1:])


if __name__ == '__main__':
    histfile = '/var/tmp/.hsh_history'
    try:
        readline.read_history_file(histfile)
    except Exception:
        pass
    atexit.register(readline.write_history_file, histfile)
    cli()
else:
    hdfs = pa.hdfs.connect()
