Hadoop Distributed Filesystem shell version 0.2
===============================================

this is a beta version of a shell, with this
you currently can browse the Hadoop Filesystem
as if you where using a regular shell...

Features
---------------------------------------------
 * it has support for command line history 
(using readline)
 * implemented commands:
   * `ls -l` the -l option is always implied
   * `cd` change base/current directory
   * `mv src1 [src2 ... srcN] dest` rename/move
files
   * `rm [-r]` delete files, optional '-r' to 
recursively delete files.

Requirements
---------------------------------------------
 * pyarrow (fs abstraction)
`pip install pyarrow`
 * readline (shell like navigation and history)
 * libhdfs (JNI based C API for HDFS)
