#!/bin/bash 

PYVER=$(basename $(readlink $(which python3)))
HSHLIB_PATH=/usr/lib/$PYVER/site-packages/hsh

mkdir -pv $HSHLIB_PATH 

install -m 0644 hsh/__main__.py $HSHLIB_PATH/__main__.py 
install -m 0755 hsh.sh /usr/bin/hsh
